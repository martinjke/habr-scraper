﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Mvc;

namespace HabrScraper.Controllers
{
    [Route("api/habr")]
    [ApiController]
    public class HabrScraperController : ControllerBase
    {
        // GET api/habr
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            string url = "https://habr.com/ru";
            var httpClient = new HttpClient();
            var htmlStr = await httpClient.GetStringAsync(url);

            var htmlDocument = new HtmlDocument();

            htmlDocument.LoadHtml(htmlStr);

            var postsHtml = htmlDocument.DocumentNode
                .SelectNodes("//ul[contains(@class,'content-list content-list_posts')]" +
                    "/li")
                .ToList();
            
            var posts = new List<Post>();

            foreach (var postHtml in postsHtml)
            {
                var post = new Post();
                var titleNode = postHtml
                    .SelectNodes(".//h2[@class='post__title']/a");
                if (titleNode != null && titleNode.FirstOrDefault() != null) {
                    post.Title = titleNode.First().InnerHtml;
                    post.Url = titleNode.First().GetAttributeValue("href", "");
                }
                else continue;
                
                var imageNodes = postHtml
                    .SelectNodes(".//img");
                if (imageNodes != null) {
                    if (imageNodes.Count > 0)
                    {
                        foreach (var image in imageNodes)
                        {
                            if (image.GetAttributeValue("src", "") != null)
                                post.ImageUrls.Add(image.GetAttributeValue("src", ""));
                        }
                    }
                }
                    
                posts.Add(post);
            }



            return Ok(new {
                count = posts.Count(),
                data = posts,
            });
        }

    }
    public class Post
    {
        public Post()
        {
            ImageUrls = new List<string>();
        }
        public string Title { get; set; }
        public string Url { get; set; }
        public List<string> ImageUrls { get; set; }
    }
}
